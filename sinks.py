"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_chars: int):
    total_chars = max_chars * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_chars, end='')
            nchars = int(max_chars * (sound[nsample] / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_chars * (-sound[nsample] / config.max_amp))
            print(' ' * (max_chars - nchars), end='')
            print('*' * nchars)

def show(sound, newline: bool):
    if newline:
        for sample in sound:
            print(sample)
    else:
        print(','.join(map(str, sound)))


def info(sound):
    num_samples = len(sound)
    max_value = max(sound)
    max_index = sound.index(max_value)
    min_value = min(sound)
    min_index = sound.index(min_value)
    mean_value = sum(sound) / num_samples
    positive_samples = sum(1 for sample in sound if sample > 0)
    negative_samples = sum(1 for sample in sound if sample < 0)
    null_samples = sum(1 for sample in sound if sample == 0)

    print(f"Samples: {num_samples}")
    print(f"Max value: {max_value} (sample {max_index})")
    print(f"Min value: {min_value} (sample {min_index})")
    print(f"Mean value: {mean_value}")
    print(f"Positive samples: {positive_samples}")
    print(f"Negative samples: {negative_samples}")
    print(f"Null samples: {null_samples}")