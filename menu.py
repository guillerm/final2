import sources
import sinks

def select_source():
    while True:
        print("Dime una fuente (load, sin, constant, square):")
        source = input().strip()
        if hasattr(sources, source):
            return source

def get_source_params(source):
    params = []
    for param in getattr(sources, source).__annotations__.keys():
        print(f"{param}:")
        value = input().strip()
        params.append(value)
    return params

def select_sink():
    while True:
        print("Dime un sumidero (play, draw, show, info):")
        sink = input().strip()
        if hasattr(sinks, sink):
            return sink

def get_sink_params(sink):
    params = []
    for param in getattr(sinks, sink).__annotations__.keys():
        if param != "sound":
            print(f"{param}:")
            value = input().strip()
            params.append(value)
    return params

def main():
    # Seleccionar fuente
    source = select_source()

    # Obtener parámetros de la fuente
    source_params = get_source_params(source)

    # Seleccionar sumidero
    sink = select_sink()

    # Obtener parámetros del sumidero
    sink_params = get_sink_params(sink)

    # Ejecutar la fuente
    try:
        sound = getattr(sources, source)(*source_params)
    except Exception as e:
        print(f"Error al ejecutar la función fuente: {e}")
        return

    # Ejecutar el sumidero
    try:
        getattr(sinks, sink)(sound, *sink_params)
    except Exception as e:
        print(f"Error al ejecutar la función sumidero: {e}")
        return

if __name__ == "__main__":
    main()
