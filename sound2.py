import sys
import sources
import processors
import sinks

def main():
    args = sys.argv[1:]  # Ignorar el primer argumento que es el nombre del programa

    # Verificar que se proporcionen suficientes argumentos
    if len(args) < 3:
        print("Uso: python3 sound2.py <source> [<processor> ...] <sink> [source_args] [processor_args] [sink_args]")
        return

    # Obtener los nombres de los módulos y sus argumentos
    source_module, *module_args = args[:args.index("sink")]
    sink_module, *sink_args = args[args.index("sink")+1:]

    # Cargar el módulo fuente
    try:
        source_func = getattr(sources, source_module)
    except AttributeError:
        print(f"No se encontró la función fuente '{source_module}'")
        return

    # Obtener los módulos procesadores
    processor_modules = []
    processor_args = []
    i = 0
    while i < len(module_args):
        if module_args[i] == "sink":
            break
        processor_modules.append(module_args[i])
        processor_args.append(module_args[i+1])
        i += 2

    # Cargar los módulos procesadores
    processor_funcs = []
    for processor_module in processor_modules:
        try:
            processor_func = getattr(processors, processor_module)
            processor_funcs.append(processor_func)
        except AttributeError:
            print(f"No se encontró la función procesadora '{processor_module}'")
            return

    # Cargar el módulo sumidero
    try:
        sink_func = getattr(sinks, sink_module)
    except AttributeError:
        print(f"No se encontró la función sumidero '{sink_module}'")
        return

    # Llamar a la función fuente y obtener el sonido
    try:
        sound = source_func(*map(eval, module_args[i+1:]))
    except Exception as e:
        print(f"Error al ejecutar la función fuente: {e}")
        return

    # Aplicar los procesadores al sonido
    for processor_func, processor_arg in zip(processor_funcs, processor_args):
        try:
            sound = processor_func(sound, *map(eval, processor_arg))
        except Exception as e:
            print(f"Error al ejecutar la función procesadora: {e}")
            return

    # Llamar a la función sumidero con el sonido procesado
    try:
        sink_func(sound, *map(eval, sink_args))
    except Exception as e:
        print(f"Error al ejecutar la función sumidero: {e}")
        return

if __name__ == "_main_":
    main()