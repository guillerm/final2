"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    out_sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        out_sound[nsample] = int(value)
    return out_sound
def shift(sound, value: int):
    out_sound = []
    for sample in sound:
        shifted_sample = sample + value
        if shifted_sample > config.max_amp:
            shifted_sample = config.max_amp
        elif shifted_sample < -config.max_amp:
            shifted_sample = -config.max_amp
        out_sound.append(shifted_sample)
    return out_sound


def trim(sound, reduction: int, start: bool):
    if reduction >= len(sound):
        return []

    if start:
        trimmed_sound = sound[reduction:]
    else:
        trimmed_sound = sound[:-reduction]

    return trimmed_sound


def repeat(sound, factor: int):
    repeated_sound = sound * factor
    return repeated_sound

def clean(sound, level: int):
    cleaned_sound = []
    for sample in sound:
        if abs(sample) < level:
            cleaned_sound.append(0)
        else:
            cleaned_sound.append(sample)
    return cleaned_sound
def round(sound):
    rounded_sound = []
    for i in range(len(sound)):
        if i == 0 or i == len(sound) - 1:
            rounded_sound.append(sound[i])
        else:
            avg = (sound[i-1] + sound[i] + sound[i+1]) // 3
            rounded_sound.append(avg)
    return rounded_sound