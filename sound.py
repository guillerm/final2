import sys
import sources
import processors
import sinks

def main():
    args = sys.argv[1:]  # Ignorar el primer argumento que es el nombre del programa

    # Verificar que se proporcionen suficientes argumentos
    if len(args) < 3:
        print("Uso: python3 sound.py <source> <processor> <sink> [source_args] [processor_args] [sink_args]")
        return

    # Obtener los nombres de los módulos y sus argumentos
    source_module, *source_args = args[:args.index("processor")]
    processor_module, *processor_args = args[args.index("processor")+1:args.index("sink")]
    sink_module, *sink_args = args[args.index("sink")+1:]

    # Cargar el módulo fuente
    try:
        source_func = getattr(sources, source_module)
    except AttributeError:
        print(f"No se encontró la función fuente '{source_module}'")
        return

    # Cargar el módulo procesador
    try:
        processor_func = getattr(processors, processor_module)
    except AttributeError:
        print(f"No se encontró la función procesadora '{processor_module}'")
        return

    # Cargar el módulo sumidero
    try:
        sink_func = getattr(sinks, sink_module)
    except AttributeError:
        print(f"No se encontró la función sumidero '{sink_module}'")
        return

    # Llamar a la función fuente y obtener el sonido
    try:
        sound = source_func(*map(eval, source_args))
    except Exception as e:
        print(f"Error al ejecutar la función fuente: {e}")
        return

    # Aplicar el procesador al sonido
    try:
        processed_sound = processor_func(sound, *map(eval, processor_args))
    except Exception as e:
        print(f"Error al ejecutar la función procesadora: {e}")
        return

    # Llamar a la función sumidero con el sonido procesado
    try:
        sink_func(processed_sound, *map(eval, sink_args))
    except Exception as e:
        print(f"Error al ejecutar la función sumidero: {e}")
        return

if __name__ == "__main__":
    main()
