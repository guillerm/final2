# Repositorio plantilla: "Proyecto final, convocatoria de junio"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.etsit.urjc.es/cursoprogram/materiales/-/blob/main/practicas/ejercicios/README.md#final2), que incluye la fecha de entrega.
